This library defines a Nuget package that provides our logging conventions, and 
console application defaults.

Sample usage:

```powershell
Install-Package Wurk.Allay
```

```csharp
internal class Program {
  private static void Main( string[] args ) {
    
    Console.WriteLine( $"Press any key to exit ..." );
    
    using ( var timer = new Timer( 2500 ) ) {
      timer.Elapsed +=
        ( sender, eventArgs ) =>
          LogTo.Information( $"The thing happened at {eventArgs.SignalTime}", eventArgs.SignalTime );
      
      timer.Start();
      Console.ReadKey();
    }
  }
}
```

That's it. Fody takes care of weaving `ModuleInit` in our library, this package includes the `LoadAssembliesOnStartup` weaver so the host application will fire the initialiser, and we tell Serilog to utilise `app.settings` based configuration. Additionally, we include a transform that merges the appropriate Serilog configuration entries into the host application's config file. _Fait accompli_

`LogTo` is now bound to a configured logger, and Serilog is happy to begin eating your events. Application configuration defaults here are to employ a literate style console logger, as this allows CI to include configuration for more advanced use, such as Elasticsearch or Rabbit MQ based sinks.