﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "Allay" )]
[assembly: AssemblyDescription("This library defines a Nuget package that provides our logging conventions, and console application defaults.")]
[assembly: AssemblyCompany( "Wurk" )]
[assembly: AssemblyProduct( "Allay" )]
[assembly: AssemblyCopyright( "Copyright - Wurk, 2016" )]
[assembly: AssemblyCulture( "" )]
[assembly: ComVisible( false )]
[assembly: Guid( "ebaea6f7-edec-45fc-b421-4262ae0c3d10" )]